import re

def HeadingLineSpace(content):
    content = re.split(r'(##.*\n)',content,flags=re.M)
    for i in range(len(content)):
        if i-1 <= 0:
            pass
        else:
            if re.match(r'(##.*\n)',content[i]) != None:
                if content[i-1] == '\n':
                    pass
                else:
                    content[i]= '<br>\n\n'+content[i]
    return ''.join(map(str,content))

def DelBulletNumSpace(content):
    content_temp =[]
    [content_temp.append(i) for i in re.split(r'(\* .*\n)',content) if i !='\n']
    content_temp =''.join(map(str,content_temp))
    content = []
    [content.append(i) for i in re.split(r'(\d\. .*\n)',content_temp) if i !='\n']
    return ''.join(map(str,content))

def Hr(content):
    content = re.split('(By the end of this unit, you will be able to:\n\n)',content)
    if len(content) >=3:
        del content[0]
        content[0] = '>**By the end of this unit, you will be able to:**\n'
        content = ''.join(map(str,content))
        tt = re.split(r'(\* .*\n)',content,flags=re.M)
        for i in range(len(tt)):
            if re.match(r'(\* .*\n)',tt[i]) != None:
                if tt[i+1] != "":
                    print (i+1)
                    tt[i]=tt[i].replace('* ', '>・')+'\n<hr style="border-top: 2px solid #eee">\n\n'
                    break
                else:
                    tt[i]=tt[i].replace('* ', '>・')
    else:
        tt = ''.join(map(str,content))
        tt = re.split(r'(\n)',tt)
        tt[0] = '<hr style="border-top: 2px solid #eee">'

                
    return ''.join(map(str,tt))
    
def Image(content):
    content = re.split(r'(.*image alt text.*image_\d*.(png|jpg|jpeg).*\n\n)',content)
    for i in range(len(content)):
        if re.match(r'(.*image alt text.*image_\d*.(png|jpg|jpeg).*\n\n)',content[i]):
            content[i] = '<div style="width:100%"> <a href="" target="_blank"><img style="max-width:700px;width:100%;" src=""></a></div>\n\n'
            content[i+1] =''
    return ''.join(map(str,content))

def Path(content):
    temp = re.split(r'(_|Source: .*?\n\n|Path: .*?\n\n)',content,flags=re.M)
    for i in range(len(temp)):
        if re.match(r'(Source: .*?\n\n|Path: .*?\n\n)',temp[i]) !=None:
            temp[i-2] = ''
            temp[i] = temp[i].replace('*\n\n','\n\n')
            temp[i] = '<p style="margin-top:10px"><span style="font-style: italic;color: #999;">'+temp[i]
            temp[i] = temp[i].replace('\n\n','</span></p>\n\n')
    return ''.join(map(str,temp))

def FurtherReading(content):
    resplit = re.split(r'(#### Key Review\n\n|#### Further Reading\n\n|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)',content,flags=re.M)
    for i in range(len(resplit)):
        if resplit[i] == '<table>\n  <thead>\n    <tr>\n      <td>' and re.match(r'#### Further Reading\n\n',resplit[i-2]) != None:
            resplit[i] = '<div style="background: #fbec9c; padding: 15px; border-radius: 10px"> \n'
            resplit[i+2] = '\n</div>'

        elif resplit[i] == '<table>\n  <thead>\n    <tr>\n      <td>' and re.match(r'#### Key Review\n\n',resplit[i-2]) != None:
            resplit[i] = '<div style="background: #fbec9c; padding: 15px; border-radius: 10px"> \n'
            resplit[i+2] = '\n</div>'
    
    return ''.join(map(str,resplit))

def Code(content):
    resplit = re.split(r'(`)',content)
    for i in range(len(resplit)):
        if i%4 == 1:
            resplit[i] = resplit[i].replace('`','<code>')
        elif i%4 == 3:
            resplit[i] = resplit[i].replace('`','</code>')
    return ''.join(map(str,resplit))

#haven't test
def Embed(content):
    resplit = re.split(r'(<iframe height="400px" width="100%" src="https://repl.it[\d\D]*?</iframe>|<p data-height="265"[\d\D]*?</script>|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)')
    for i in range(len(resplit)):
        if resplit[i] == '<table>\n  <thead>\n    <tr>\n      <td>' and re.match('<p data-height="265"[\d\D]*?</script>',resplit[i+1]) != None:
            resplit[i] = ''
            resplit[i+2] = ''
        elif resplit[i] == '<table>\n  <thead>\n    <tr>\n      <td>' and re.match('<iframe height="400px" width="100%" src="https://repl.it[\d\D]*?</iframe>',resplit[i+1]) != None:
            resplit[i] = ''
            resplit[i+2] = ''
    return ''.join(map(str,resplit))



def Terminal(content):
    resplit = re.split(r'(\$|#|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>|\n)',content)
    for i in range(len(resplit)):
        if resplit[i] == '$' and resplit[i-2] == '<table>\n  <thead>\n    <tr>\n      <td>':
            resplit[i-2] = resplit[i-2].replace('<table>\n  <thead>\n    <tr>\n      <td>','<pre style="background:#000;color:#fff;font-size:15px;line-height:1.6">')
            print(i)
            for end in range(i,len(resplit)):
                if resplit[end] == '</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>':
                    break
            for t in range(i,end+1):
                if resplit[t] == '$':
                    if resplit[t-1] != '':
                        resplit[t-1] = '<span style="color:#7dee00">'+resplit[t-1]+'</span>'

                elif resplit[t] == '#':
                    resplit[t] = resplit[t].replace('#','<span style="color:yellow">#')
                    resplit[t+1] = resplit[t+1] + '</span>'
            resplit[end] = resplit[end].replace('</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>','\n</pre>')
    return ''.join(map(str,resplit))
                        
                    

def warning(content):
    resplit = re.split(r'(#### Warning\n\n|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)',content)
    for i in range(len(resplit)):
        if resplit[i] == '#### Warning\n\n':
            resplit[i+2] = ''
            resplit[i+3] = '<div style="background: #fbec9c; padding: 15px; border-radius: 10px">\n  ' + resplit[i+3] + '\n</div>\n\n'
            resplit[i+4] = ''

    return ''.join(map(str,resplit))


def Answer(content,file_name):
    a_id = 1
    resplit = re.split(r'(<br>\n\n#### Answer\n\n|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)',content)
    for i in range(len(resplit)):
        if resplit[i] == '<br>\n\n#### Answer\n\n':
            resplit[i] = resplit[i].replace('<br>\n\n#### Answer\n\n','<a class="btn btn-start" role="button" data-toggle="collapse" href="#'+ ''.join(map(str,file_name.split('.md'))) + '-' + str(a_id) + '" aria-expanded="false" aria-controls="' + ''.join(map(str,file_name.split('.md'))) + '-' + str(a_id) + '">Answer</a>\n\n')
            resplit[i+2] = ''
            resplit[i+3] = '<div class="collapse" id="'+ ''.join(map(str,file_name.split('.md'))) + '-' + str(a_id) + '">\n  <div class="well">\n    ' + resplit[i+3] + '\n  </div>\n</div>\n\n'
            resplit[i+4] = ''
            a_id += 1

    return ''.join(map(str,resplit))
    


# haven't test
def SampleCode(content):
    resplit = re.split(r'(#### Sample Code|<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)',content)
    for i in range(len(resplit)):
        if resplit[i] == '#### Sample Code':
            resplit[i+2] = '<div style="background: #fbec9c; padding: 15px; border-radius: 10px">\n  '
            resplit[i+4] = '\n</div>\n\n'
    return ''.join(map(str,resplit))

def Quotes(content):
    content = content.replace('‘',"'")
    content = content.replace('’',"'")
    content = content.replace('“','"')
    content = content.replace('”','"')
    return content