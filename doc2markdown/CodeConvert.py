from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
import re
import requests

def html_convert(code, lexer, style, divstyles):
    """For future, switch to offline converting"""

    lexer = lexer
    style = style or 'colorful'
    defstyles = 'overflow:auto;width:auto;'

    formatter = HtmlFormatter(style=style,
                              noclasses=True,
                              cssclass='',
                              cssstyles=defstyles + divstyles,
                              prestyles='margin: 0')
    html = highlight(code, get_lexer_by_name(lexer), formatter)

    return html

def ac_convert(file):
    """ file is markdown file ready to convert, insert a str data type"""
    
    code = file
    style = 'autumn'
    divstyles = 'border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;'
    resplit = re.split(r'(<table>\n  <thead>\n    <tr>\n      <td>|</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>)',code,flags=re.M)
    for i in range(len(resplit)):
        if resplit[i] == '</td>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>':
            resplit[i-2] = None
            resplit[i]=None
    temp = []
    [temp.append(i) for i in resplit if i != None and i != '']
    resplit = ''.join(map(str, temp))
    resplit = re.split('(#codeblock)|#/codeblock|!!!',resplit)
    html = []
    [html.append(i) for i in resplit if i != None and i != '']
    while '#codeblock' in html:
        lexer = html[html.index('#codeblock')+1]
        code_num = html.index('#codeblock')+2
        code_temp =html[code_num]
        params = {
            'code': code_temp,
            'lexer': lexer,
            'options': {},
            'style': 'autumn',
            'divstyles': '"background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;'
        }
        html[code_num] = requests.post('http://hilite.me/api', data = params).text
        html.remove('#codeblock')
        html.remove(lexer)  #re.remove()
        default_s = '<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;"background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">'
        AC_style = '\n<pre style="background:#f9f9f9;color:#080808">'
        html[code_num-2] = html[code_num-2].replace(default_s,AC_style)
        div = '</pre></div>'
        pre = '</pre>'
        html[code_num-2] = html[code_num-2].replace(div,pre)
        
    html = ''.join(map(str, html))
    return html