## AC Content Deployment tool

---

A tool for deploying content from google doc to lighthouse version 1.

Features:

* Convert Google doc contaxt to markdown markup file
* Add AC style markup
* Convert code block via [hilite.me](http://hilite.me/) API

## Steps

---

* Copy the script of **google_convert.js** and paste on google doc **tool > scrpit editor**
```
doc2markdown
|
|_**google_convert.js**
```

* Run the **ConvertToMarkdown** function.

* Check your Email and download all the files

* Move the **.md** file to **original_files**

* Run **main.py**

* You will get the converted **.md** file in **md_files**

* Upload images to lighthouse paste url in the **.md** file

* Copy script of **.md** to Lighthouse.


## Sepcial reminder

---

* The tool only support by Python 3. Please install Python 3. (Recommand way [ANACONDA](https://www.anaconda.com/download/#macos))
