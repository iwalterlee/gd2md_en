from doc2markdown import doc2markdown, CodeConvert
import os

dir = os.getcwd()
lib = os.listdir(dir+'/original_files')
files = []
[files.append(i) for i in lib if i != '.DS_Store']

for i in files:
    db = open(dir+'/original_files/'+i,'r').read()
    db = doc2markdown.Quotes(db)
    db = doc2markdown.HeadingLineSpace(db)
    db = doc2markdown.DelBulletNumSpace(db)
    db = doc2markdown.Hr(db)
    db = doc2markdown.Image(db)
    db = doc2markdown.Path(db)
    db = doc2markdown.FurtherReading(db)
    #db = doc2markdown.Code(db)
    db = doc2markdown.Answer(db,i)
    db = doc2markdown.Terminal(db)
    db = doc2markdown.warning(db)
    db = doc2markdown.SampleCode(db)
    #db = doc2markdown.Embed(db)
    db = CodeConvert.ac_convert(db)
    result = open(dir+'/md_files/'+i,'w')
    result.write(db)
    result.close()